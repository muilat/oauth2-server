'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    // firstName: DataTypes.STRING,
    // middleName: DataTypes.STRING,
    // lastName: DataTypes.STRING,
    // avatar: DataTypes.STRING,
    // email: DataTypes.STRING,
    // phone: DataTypes.STRING,
    password: DataTypes.STRING,
    // roleId: DataTypes.ENUM('isSuperAdmin', 'isAdmin' , 'isEmployee'),
    // userRoleId: DataTypes.INTEGER,
  }, {});

//   User.associate = function(models) {
//     // associations can be defined here
//     User.belongsTo(models.PayGrade, {foreignKey: 'gradeId', as: 'grade'});
//     User.belongsTo(models.Branch, {foreignKey: 'branchId', as: 'branch'});
//     User.belongsTo(models.CompanyStructure, {foreignKey: 'companyStructureId', as: 'companyStructure'});
//     User.belongsTo(models.UserRole, { foreignKey: 'userRoleId', as: 'userRole'});

//     User.hasOne(models.EmployeeJob, {foreignKey: 'userId', as : 'employeeJob'})
//     // User.hasMany(models.EmployeeJob, {foreignKey:userId, as : 'employeeJobs'})
//     User.hasOne(models.EmployeeSalary, {foreignKey: 'userId', as : 'salary'})
//     User.hasMany(models.EmployeeSalary, {foreignKey: 'userId', as : 'salaryHistories'})
//     // User.hasOne(models.EmployeeSalaryComponent, {foreignKey: 'userId', as : 'salaryComponent'})

//   };


  return User;
};